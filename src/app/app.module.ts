import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { routes } from "./modules/routing/routing.module";
import { MarkdownModule } from "ngx-markdown";

import { AppComponent } from './app.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { HeaderComponent } from './components/header/header.component';
import { LandingComponent } from './pages/landing/landing.component';
import { FooterComponent } from './components/footer/footer.component';
import { ResumeComponent } from './pages/resume/resume.component';
import { RouterModule } from '@angular/router';
import { SkillsComponent } from './components/skills/skills.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { BlogComponent } from './pages/blog/blog.component';
import { ArticleComponent } from './pages/article/article.component';
import { ProjectDetailsComponent } from './pages/project-details/project-details.component';
import { ProjectThumbComponent } from './components/project-thumb/project-thumb.component';
import { ArticleThumbComponent } from './components/article-thumb/article-thumb.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ProgressBarComponent,
    MainNavComponent,
    HeaderComponent,
    LandingComponent,
    FooterComponent,
    ResumeComponent,
    SkillsComponent,
    ProjectsComponent,
    BlogComponent,
    ArticleComponent,
    ProjectDetailsComponent,
    ProjectThumbComponent,
    ArticleThumbComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {useHash: true}),
    MarkdownModule.forRoot({loader: HttpClient})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
