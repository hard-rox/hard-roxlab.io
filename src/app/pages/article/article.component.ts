import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/ext-script-loader';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  public article: Article;

  constructor(private titleService: Title,
    private metaService: Meta,
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleService) { }

  ngOnInit() {
    ExternalScriptLoader.loadScript();
    let articleId = +this.activatedRoute.snapshot.paramMap.get("id");

    this.article = this.articleService.getArticle(articleId);
    this.titleService.setTitle(this.article.title + " - Rasedur Rahman Roxy");

    this.metaService.updateTag({name: "title", content: this.titleService.getTitle()});
    this.metaService.updateTag({name: "description", content: this.article.title});
    this.metaService.updateTag({name: "og:image", content: this.article.coverImageUrl});
    this.metaService.updateTag({name: "twitter:image", content: this.article.coverImageUrl});
  }

}
