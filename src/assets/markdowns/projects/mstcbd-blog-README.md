# Microsoft Technical Community BD Event Management

**Microsoft Technical Community BD Event Management** is an ASP.NET Core web app for Microsoft Technical Community BD. This app offers the features of Managing Contributors, Speakers, Events, Registrations of audience for events hosted by Community.

### Prerequisites

Things have to installed to continue...
1. [DotNet Core SDK 3.0.0 or higher](https://dotnet.microsoft.com/download).
2. [MS SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads).
Pretty much it.


## Built With

* [DotNet Core SDK 3.0.0](https://dotnet.microsoft.com/download) - The framework used
* [MS SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads). - Database Engine
* [Visual Studio 2017](https://visualstudio.microsoft.com/downloads/) - IDE
* [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017) - Database Management Studio
* [Git](https://git-scm.com/downloads) - For versioning.

## Authors

* **Rasedur Rahman Roxy** - *Planning and Development* - [Twitter](https://twitter.com/roxyxmw?lang=en)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* [ColorLib](https://colorlib.com) for their opensourced templates.
